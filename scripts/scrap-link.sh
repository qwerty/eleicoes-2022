EST=$1
MUN=$2
ZON=$3
SEC=$4

FOLDER_PATH="$HOME/eleicoes-2022/links"
FILE_PATH="$FOLDER_PATH/$EST.txt"

echo "EST: $EST, MUN: $MUN, ZON: $ZON, SEC: $SEC" 1>&2
echo "https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/dados/$EST/$MUN/$ZON/$SEC/p000406-$EST-m$MUN-z$ZON-s$SEC-aux.json" \
		| xargs curl -s \
		| jq -r '.hashes[] | "\(.hash)/\(.nmarq[])"' \
		| xargs -I{} echo "https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/dados/$EST/$MUN/$ZON/$SEC/{}" >> $FILE_PATH
