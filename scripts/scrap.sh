ESTADO=$1
FILE_PATH="$HOME/eleicoes-2022/scripts/scrap-link.sh"

curl -s https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/config/$ESTADO/$ESTADO-p000406-cs.json \
		| jq -r '.abr[].mu[] | "\(.cd) \(.zon[].cd) \(.zon[].sec[].ns)"' \
		| parallel --colsep ' ' -j 24 -l "sh $FILE_PATH $ESTADO {1} {2} {3}"
